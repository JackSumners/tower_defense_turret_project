﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BDefender : MonoBehaviour
{

   

    public double health;
    public Transform target;
    //public double DPS;
    //public double DPSO;

    private Vector3 direction;



    public void OnCollisionStay(Collision col)
    {
        if (col.gameObject.tag == "Attacker")
        {
            BAttacker _hitDamage = col.gameObject.GetComponent<BAttacker>();
            health -= (_hitDamage.DPSO/ 60);
            if (health < 1)
            {
                Destroy(this);
                Destroy(gameObject);
            }
        }

    }
}