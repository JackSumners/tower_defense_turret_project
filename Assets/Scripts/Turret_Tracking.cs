﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Turret_Tracking : MonoBehaviour {

    // Use this for initialization
    public Transform target;
    public float speed;

    public double DPSO;
    public double AttackSpeed;
    public double Health;
    public double viewDist;
    public Transform turretBarrel;

    BAttacker AttackTarget;


    //DISPLAYS
    public double distanceB;

    public GameObject objEnemy;
    
    private bool death;
    private Vector3 direction;
    private int countT;
    private GameObject TargetObj;
    private double LastTime;
    private bool previousTarget;


    //private LineRenderer LineOfSight;

    // Use this for initialization
    void Start()
    {
        //LineOfSight = GetComponent<LineRenderer>();
        LastTime = Time.time;
        previousTarget = false;
        death = false;

        //target.transform.hasChanged = false;  //since we change transform in public(unity) lets ignore it!
        //TargetObj = target.transform.gameObject;
        countT = 1;


    }

    // Update is called once per frame
    void Update()
    {
        //RaycastHit hit;


        if (target != null && Vector3.Distance(this.transform.position, target.transform.position) <= viewDist)
        {
            distanceB = Vector3.Distance(this.transform.position, target.transform.position);  // just for show

            TargetObj = target.transform.gameObject;
            previousTarget = true;

            // if the target remains unchanged due to discovery issues
            
                transform.LookAt(target, Vector3.up);
                if (target.tag == "Attacker")
                {
                    if (Time.time - LastTime >= AttackSpeed)
                    {
                    LastTime = Time.time;

                    //if (Physics.Raycast(turretBarrel.transform.position, turretBarrel.transform.forward, out hit, (int)viewDist))
                    //{}//float distanceToGround = hit.distance;
                    ///if (hit.collider.tag == "Attacker")
                    //{
                    // Debug.Log(target.name + " Hit");


                    AttackTarget = (BAttacker)target.gameObject.GetComponent(typeof(BAttacker));

                    AttackTarget.TurretDamage(DPSO,ref death); //ref neds to be used in call and funct. param.

                            if (death) 
                            {
                        Debug.Log("Target " + target.gameObject.name + " Death => Null");
                                TargetObj = null;
                                death = false;
                            } // if target dies to turret dmg/ set target to null after destroy

                        
                        //}

                        
                    }

                }
        }
        else 
        {
            Debug.Log("Target => NULL");
            TargetObj = null; 
        }


        /*
        //if -- target.transform.hasChanged
        if (TargetObj == null && previousTarget == true && (GameObject.Find("BA (" + countT + ")")))
        {
            Debug.Log("Start Target Switch");
            target = GameObject.Find("BA (" + countT + ")").transform;
            TargetObj = target.transform.gameObject;
            countT++;
            Debug.Log("Start Target Switch => " + target.name );
        }
        */

        //if -- target.transform.hasChanged
        if (TargetObj == null && (GameObject.FindWithTag("Attacker")))
        {
            Debug.Log("Start Target Switch");
            target = GetClosestEnemy();

            if (Vector3.Distance(this.transform.position, target.transform.position) <= viewDist)
            {
                TargetObj = target.gameObject;
                countT++;
                Debug.Log("Target Switched Closest => " + target.name);
            }
        }

    }

    

    Transform GetClosestEnemy()
    {
        Debug.Log("Finding Closest");
        Transform tMin = null;
        float minDist = Mathf.Infinity;
        Vector3 currentPos = transform.position;

        var children = objEnemy.GetComponentsInChildren<Transform>();
        foreach (var child in children) 
        {
            float dist = Vector3.Distance(child.position, currentPos);
            if (dist < minDist)
            {
                tMin = child;
                minDist = dist;
            }
        }
        Debug.Log("Closest is " + tMin);
        return tMin;
    }


}
